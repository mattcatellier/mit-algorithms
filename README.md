# MIT Algorithms #

http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-introduction-to-algorithms-sma-5503-fall-2005/video-lectures/

### What is this repository for? ###

This is my code samples from algorithms introduced in the above lectures. Will include a folder with all my notes in html format once complete.

### Questions? ###
mattcatellier.com